<?php
namespace Datenbetrieb\Berichtsheft\Controller;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "Datenbetrieb.Berichtsheft".*
 *                                                                        *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Mvc\Controller\ActionController;
use Datenbetrieb\Berichtsheft\Domain\Model\ReportPage;

class ReportPageController extends ActionController {

	/**
	 * @Flow\Inject
	 * @var \Datenbetrieb\Berichtsheft\Domain\Repository\ReportPageRepository
	 */
	protected $reportPageRepository;

	public function initializeAction() {
		$mostRecent = $this->reportPageRepository->findMostRecent();
		if($mostRecent === NULL) {
			$startDate = new \DateTime('20120903');
		}
		else {

			$startDate = $mostRecent->getStartDateTime();
			/*
			 * we have to add 1 week to the last entry here because we dont want to add an entry for the existing week.
			 */
			$startDate->add(new \DateInterval("P1W"));

		}
		$endDate = new \DateTime();


		$periods = \Datenbetrieb\Berichtsheft\Services\TrainingCalendarService::getAllWeeksStartDates($startDate, $endDate);
		foreach ($periods as $period) {
			$reportPage = $this->objectManager->get('Datenbetrieb\Berichtsheft\Domain\Model\ReportPage');
			$reportPage->setYear($period->format("o"));
			$reportPage->setWeek($period->format("W"));
			$this->reportPageRepository->add($reportPage);
		}
		$persistenceManager = $this->objectManager->get('TYPO3\Flow\Persistence\PersistenceManagerInterface');
		$persistenceManager->persistAll();


	}

	/**
	 * @return void
	 */
	public function indexAction() {
		$this->view->assign('reportPages', $this->reportPageRepository->findAll());
	}

	/**
	 * @param \Datenbetrieb\Berichtsheft\Domain\Model\ReportPage $reportPage
	 * @return void
	 */
	public function showAction(ReportPage $reportPage) {
		$this->view->assign('reportPage', $reportPage);
	}

	/**
	 * @return void
	 */
	public function newAction() {
	}

	/**
	 * @param \Datenbetrieb\Berichtsheft\Domain\Model\ReportPage $reportPage
	 * @return void
	 */
	public function editAction(ReportPage $reportPage) {
		$this->view->assign('reportPage', $reportPage);
	}

	public function listAction() {
		$allEntries =  $this->reportPageRepository->findAll();
		$this->view->assign('reportPages', $allEntries);
	}

	/**
	 * @param \Datenbetrieb\Berichtsheft\Domain\Model\ReportPage $reportPage
	 * @return void
	 */
	public function updateAction(ReportPage $reportPage) {
		$this->reportPageRepository->update($reportPage);
		$this->redirect('list');
	}

	/**
	 * @param \Datenbetrieb\Berichtsheft\Domain\Model\ReportPage $reportPage
	 * @return void
	 */
	public function deleteAction(ReportPage $reportPage) {
		$this->reportPageRepository->remove($reportPage);
		$this->addFlashMessage('Deleted a report page.');
		$this->redirect('index');
	}

}
