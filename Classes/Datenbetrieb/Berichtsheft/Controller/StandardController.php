<?php
namespace Datenbetrieb\Berichtsheft\Controller;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "Datenbetrieb.Berichtsheft".*
 *                                                                        *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Mvc\Controller\ActionController;
use Datenbetrieb\Berichtsheft\Domain\Model\ReportPage;

class StandardController extends ActionController {


	public function indexAction() {
		$this->redirect('index', 'ReportPage', 'Datenbetrieb.Berichtsheft');
	}
}
