<?php
/**
 * Created by PhpStorm.
 * User: max
 * Date: 03.04.14
 * Time: 09:47
 */

namespace Datenbetrieb\Berichtsheft\Services;


class TrainingCalendarService {


	/**
	 * @return \DatePeriod
	 */
	public static function getAllWeeksStartDates(\DateTime $pStartDate, \DateTime $pEndDate) {
		$startDate = clone $pStartDate;
		$endDate = clone $pEndDate;
		$startDate->setTime(0,0,0);
		$endDate->setTime(0,0,0);
		/*
		 * We have to add one week to the end date because we want to create an entry for the last non existing week
		 */
		$endDate->add(new \DateInterval("P1W"));
		$startDate->setISODate($startDate->format("Y"), $startDate->format("W"), 1);
		$endDate->setISODate($endDate->format("Y"), $endDate->format("W"), 1);
		$interval = \DateInterval::createFromDateString('1 week');
		$period = new \DatePeriod($startDate, $interval, $endDate);

		/*foreach($period as $p) {
			echo "First Monday of week " . $p->format('W') . " has the date: " . $p->format('d.m.Y') . "\n";
		}*/
		return $period;

	}

}
