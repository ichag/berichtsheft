<?php
namespace Datenbetrieb\Berichtsheft\Domain\Repository;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "Datenbetrieb.Berichtsheft".*
 *                                                                        *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Persistence\QueryInterface;
use TYPO3\Flow\Persistence\Repository;

/**
 * @Flow\Scope("singleton")
 */
class ReportPageRepository extends Repository {

	// add customized methods here
	protected $defaultOrderings = array('year' => QueryInterface::ORDER_DESCENDING,
									   'week' => QueryInterface::ORDER_DESCENDING);

	public function findMostRecent() {
		$query = $this->createQuery();
		return $query->setOrderings(array(
		  'year' => QueryInterface::ORDER_DESCENDING,
		  'week' => QueryInterface::ORDER_DESCENDING))
		  ->execute()->getFirst();
	}

}
