<?php
namespace Datenbetrieb\Berichtsheft\Domain\Model;

/*                                                                        *
 * This script belongs to the TYPO3 Flow package "Datenbetrieb.Berichtsheft".*
 *                                                                        *
 *                                                                        */

use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class ReportPage {

	/**
	 * @var string
	 * @ORM\Column(length=65535)
	 */
	protected $companyReport = '';

	/**
	 * @var string
	 * @ORM\Column(length=65535)
	 */
	protected $instructions = '';

	/**
	 * @var string
	 * @ORM\Column(length=65535)
	 */
	protected $businessSchoolReport = '';

	/**
	 * @var integer
	 *
	 */
	protected $year;

	/**
	 * @var integer
	 *
	 */
	protected $week;

	/**
	 * @return string
	 */
	public function getCompanyReport() {
		return $this->companyReport;
	}

	/**
	 * @param string $companyReport
	 * @return void
	 */
	public function setCompanyReport($companyReport) {
		$this->companyReport = $companyReport;
	}

	/**
	 * @param string $instructions
	 * @return void
	 */
	public function setInstructions($instructions) {
		$this->instructions = $instructions;
	}

	/**
	 * @return string
	 */
	public function getInstructions() {
		return $this->instructions;
	}

	/**
	 * @return string
	 */
	public function getBusinessSchoolReport() {
		return $this->businessSchoolReport;
	}

	/**
	 * @param string $businessSchoolReport
	 * @return void
	 */
	public function setBusinessSchoolReport($businessSchoolReport) {
		$this->businessSchoolReport = $businessSchoolReport;
	}

	/**
	 * @return integer
	 */
	public function getYear() {
		return $this->year;
	}

	/**
	 * @param integer $year
	 * @return void
	 */
	public function setYear($year) {
		$this->year = $year;
	}

	/**
	 * @return integer
	 */
	public function getWeek() {
		return $this->week;
	}

	/**
	 * @param integer $week
	 * @return void
	 */
	public function setWeek($week) {
		$this->week = $week;
	}

	/**
	 * @return \DateTime
	 */
	public function getStartDateTime() {
		$dateTime = new \DateTime();
		$dateTime->setTime(0,0,0);
		$dateTime->setISODate($this->getYear(), $this->getWeek(), 1);
		return $dateTime;
	}

	public function getEndDateTime() {
		$dateTime = new \DateTime();
		$dateTime->setTime(0,0,0);
		$dateTime->setISODate($this->getYear(), $this->getWeek(), 5);
		return $dateTime;
	}

}
