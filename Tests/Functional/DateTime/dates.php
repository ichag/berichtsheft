<?php
$startOn = new DateTime('20120903');
$startYear = $startOn->format('Y');
$now = new DateTime('');
$endYear = $now->format('Y');

$years = range($startYear, $endYear);
$date=new DateTime();
$currentWeek = $date->format('W');
echo "Aktuelle Kalenderwoche: " . $currentWeek . "\n";
foreach ($years as $year) {
    if ($year == $endYear) {
        $lastWeek = $currentWeek;
    } else {
        $date->setDate($year, 12, 28);
        $lastWeek = $date->format('W');
    }
    if ($startOn->format('Y') == $year) {
        $firstWeek = $startOn->format('W');
    } else {
        $firstWeek = 1;
    }

    foreach (range($firstWeek, $lastWeek) as $weekNumber) {
       $date->setISODate($year, $weekNumber, 1);
       echo 'Jahr ' . $year . ' Kalenderwoche ' . $weekNumber . ' hat Montag den ' . $date->format('d.m.Y') . "\n";
    }
}
